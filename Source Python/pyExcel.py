#PyExcelSerch




###Import Modules##
import xlrd
import os
import sys
from PyQt4 import QtGui
from PyQt4.QtCore import QThread, SIGNAL
import gui
import chardet


####################


class SearchThread(QThread):
    def __init__(self,keyword,files):
        QThread.__init__(self)
        self.keyword = keyword
        self.files = files
        
    def __del___(self):
        self.wait()


    def google(self,keyword):
        if self.keyword == keyword:
            return True 
    
    def run(self):
        new_file = 0
        for fname in self.files:
            new_file += 1
            self.emit(SIGNAL('progress(int)'), new_file)
            a = open(fname).read()
            encodeing = chardet.detect(a)
            workbook_xlrd = xlrd.open_workbook(fname,encoding_override=encodeing)
            for x in range(0,workbook_xlrd.nsheets):
                for x2 in range(0,workbook_xlrd.sheet_by_index(x).ncols):
                    row = 0
                    for cell in workbook_xlrd.sheet_by_index(x).col(x2):
                        row = row + 1
                        if self.google(cell.value) == True:
                            add_item = "Was in row " + str(row) + " column " + str(x2+1) + " of file " + fname
                            self.emit(SIGNAL("add_item(QString)"), add_item)

class GUI(QtGui.QMainWindow, gui.Ui_MainWindow):
    def __init__(self):
        super(GUI, self).__init__()
        self.setupUi(self)
        self.progressBar.setValue(0)
        self.pushButton.clicked.connect(self.start_search)
        self.pushButton_3.clicked.connect(self.reset)


    def start_search(self):
        self.files_xl = []
        os.chdir(str(self.directory()))
        files = os.listdir(str(self.directory()))
        for f in files:
            file_extension = f.split(".")[-1]
            if self.extension_check(file_extension) == True:
                self.files_xl.append(f)
        self.progressBar.setMaximum(len(self.files_xl))
        self.progressBar.setValue(0)
        self.SearchThread = SearchThread(self.keyword(),self.files_xl)

        self.connect(self.SearchThread, SIGNAL("add_item(QString)"),self.add_item)
        self.connect(self.SearchThread, SIGNAL("finished()"), self.done)
        self.connect(self.SearchThread, SIGNAL("progress(int)"), self.progress)
        self.SearchThread.start()
        self.pushButton_2.setEnabled(True)
        self.pushButton_2.clicked.connect(self.SearchThread.terminate)
        self.pushButton.setEnabled(False)

    def add_item(self,add_item):
        self.listWidget.addItem(add_item)

    def reset(self):
        self.listWidget.clear()

    def progress(self,progress):
        self.progressBar.setValue(progress)
        
    def done(self):
        self.pushButton_2.setEnabled(False)
        self.pushButton.setEnabled(True)
        self.progressBar.setValue(0)
        QtGui.QMessageBox.information(self, "Done!", "Done Searching Directory ! " + str(self.directory()))

                
    def directory(self):
        return self.lineEdit_2.text()
    
    def keyword(self):
        return self.lineEdit.text()

    def extension_check(self,file_extension):
        if file_extension == "xlsx":
                return True 
        if file_extension == "xls":
                return True





    
    
    
        

def main():
    app = QtGui.QApplication(sys.argv)
    form = GUI()
    form.show()
    app.exec_()

if __name__ == "__main__":
    main()














        
